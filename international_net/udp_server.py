from socket import *

server_port = '10086'
serve_socket = socket(AF_INET, SOCK_DGRAM)
serve_socket.bind('', server_port)


while True:
    message, client_addr = serve_socket.recvfrom(2048)
    # 对message进行数据处理
    serve_socket.sendto(message.encode(), client_addr)
