https://blog.csdn.net/qq_43139145/article/details/128073474

## 1.安装Django

```python
pip install django
```

```
c:\python36
	- python.exe
	- Scripts
		- pip.exe
		- django-admin.exe  [工具，创建django项目中的文件和文件夹]
	- Lib
		- 内置模块
		- site-packages
			- openpyxl
			- python-docx
			- flask
			- django    [框架源码]
```

## 2. 创建项目

> django中项目会有一些默认的文件和文件夹

### 2.1 在终端

* 打开终端
* 进入项目目录
* 执行命令创建项目
```python
django-admin startproject 项目名称
``` 

```
PS C:\Users\23111\Desktop\test\ngot> django-admin startproject django_code
PS C:\Users\23111\Desktop\test\ngot> ls


    目录: C:\Users\23111\Desktop\test\ngot


Mode                 LastWriteTime         Length Name
----                 -------------         ------ ----
d-----         2023/7/26     23:08                django_code
d-----         2023/7/26     22:55                django_note
d-----         2023/3/28     23:28                git_study
d-----         2023/3/28     23:28                international_net
d-----         2022/6/14      0:44                Mobil_AUTO
d-----         2023/7/26     22:48                yangyang_04.19
-a----         2023/3/28     23:35              0 abcd
-a----         2023/3/28     23:24              0 hello.txt
```

### 2.2 Pycharm

* 略(企业版)

### 2.3 终端与pycharm创建项目比较

* 命令行创建的是标准的
* pycharm在标准的基础上默认添加配置
	* 创建templates目录
	* settings添加部分信息


### 默认项目文件介绍

```
mysite
	- manage.py     [项目管理:启动项目,创建app,数据管理]
	- mysite
		- __init__.py    []
		- settings.py    [项目配置文件]
		- urls.py    [URL和函数的对应关系]
		- wsgi.py    [接收网络请求][不动]
		- asgi.py    [接收网络请求][不动]
```

## 3. APP

```
- 项目
	- app(用户管理)
	- app(后台管理)
	- app(网站)
	- app(API)
	...
```

```python 
python manage.py startapp app01
```

```
- app01
	- __init__.py
	- admin.py    [固定,不动] django默认提供admin后台管理
	- apps.py    [固定,不动] app启动类
	- migrations    [固定,不动] 数据库变更记录
		- __init__.py
	- models.py    [重要] 数据库操作
	- tests.py    [单元测试]
	- views.py    [重要] 视图函数
```

汇总
```
mysite
	- manage.py     [项目管理:启动项目,创建app,数据管理]
	- mysite
		- __init__.py    []
		- settings.py    [项目配置文件]
		- urls.py    [URL和函数的对应关系]
		- wsgi.py    [接收网络请求][不动]
		- asgi.py    [接收网络请求][不动]
	- app01
		- __init__.py
		- admin.py    [固定,不动] django默认提供admin后台管理
		- apps.py    [固定,不动] app启动类
		- migrations    [固定,不动] 数据库变更记录
			- __init__.py
		- models.py    [重要] 数据库操作
		- tests.py    [单元测试]
		- views.py    [重要] 视图函数
```

## 4. 快速上手

* 确保APP已注册【settings】
![[Pasted image 20230726234143.png]]
* 编写URL和视图函数对应关系 【urls.py】
![[Pasted image 20230726234452.png]]
* 编写视图函数 【views.py】
![[Pasted image 20230726234726.png]]
* 启动django项目
	* 命令行启动
	```python
	python manage.py runserver 端口
```
![[Pasted image 20230726235011.png]]
