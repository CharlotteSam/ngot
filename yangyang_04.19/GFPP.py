# 打开文件（是当前只支持单个文件）
f = open(r'C:\Users\juzi\Desktop\data_structure\FOFS_GFPP.pdb', 'r')


PHE_or_TRP_residue = {}    # 满足过滤条件（'CG' in data）的PHE/TRP字典
LEU_residue = {}    # 满足过滤条件（'CG' in data）的LEU字典
GFPP_residue = {}    # 满足过滤条件（'CG' in data）的GFPP字典

ok_PHE_or_TRP_residue = {}  # 满足PHE/TRP与LEU距离<7的PHE_or_TRP_residue
ok_LEU_residue = {}  # 满足PHE/TRP与LEU距离<7的LEU_residue
ok_dict = {}    # 满足条件的PHE/TRP与LEU组成的字典 key为PHE/TRP,value为对应的LEU
# ok_list =[]    # 考虑到写入ok_dict时key为PHE/TRP,value为对应的LEU可能出现单个key存在多个value的情况引入了该list


for line in f.readlines():
    data = line.split()
    # line为读取的一行信息，如下
    # 'ATOM   5038  HN  PHE A 323     -55.995  12.217   0.817  1.00  0.00           H  '  此处的格式为str
    # line.split()为将字符串按照特定的规则切割，这里split()里面为空，默认为按照空格分割，若split('/')表示按照‘/’分割，详情可以百度
    # 分割后的结果，类型为list,如下
    # ['ATOM', '5038', 'HN', 'PHE', 'A', '323', '-55.995', '12.217', '0.817', '1.00', '0.00', 'H']
    if 'PHE' in data and 'CG' in data:
        # 此处为写入字典操作
        # dict
        # key :PHE_323
        # value: [-55.995,12.217,0.817] ,value的数据类型为list,以下操作同理
        PHE_or_TRP_residue[f'{data[3]}_{data[5]}'] = [
            float(data[6]), float(data[7]), float(data[8])]
    elif 'TRP' in data and 'CG' in data:
        PHE_or_TRP_residue[f'{data[3]}_{data[5]}'] = [
            float(data[6]), float(data[7]), float(data[8])]
    elif 'LEU' in data and 'CG' in data:
        LEU_residue[f'{data[3]}_{data[5]}'] = [
            float(data[6]), float(data[7]), float(data[8])]
    elif 'HETATM' in data and len(data) == 10:
        GFPP_residue[f'{data[2]}_{data[1]}'] = [
            float(data[4]), float(data[5]), float(data[6])]
# print(LEU_residue)
# print(GFPP_residue)

# 设置PHE/TRP与LEU的距离
distance_cutoff = 7
# print(distance_cutoff)

print(
    'PHE/TRP的CG距离LEU的CG小于{distance_cutoff}A'.format(distance_cutoff=distance_cutoff))
print('/******************************/')
# 遍历PHE_or_TRP_residue的key
for i in PHE_or_TRP_residue:
    #  i 为PHE_or_TRP_residue字典的key
    # PHE_or_TRP_residue[i]取PHE_or_TRP_residue的value
    # {'PHE_100':[10,555,855],'PHE_101':[11,455,855],......}
    # 第一个[]为根据key的值获取字典的value
    # PHE_or_TRP_residue[i]=[10,555,855]
    # 第二个[]的含义，因为value的类型为list,坐标xyz分别对应列表的的0，1，2号数据，这里的[]为取list的值
    # PHE_or_TRP_residue[i][0] = 10
    PHE_or_TRP_x = PHE_or_TRP_residue[i][0]
    PHE_or_TRP_y = PHE_or_TRP_residue[i][1]
    PHE_or_TRP_z = PHE_or_TRP_residue[i][2]

    # 嵌套循环，比较坐标的距离
    for j in LEU_residue:
        LEU_x = LEU_residue[j][0]
        LEU_y = LEU_residue[j][1]
        LEU_z = LEU_residue[j][2]
        # 比较PHE与LEU的距离是否满足条件
        distance_PHE_or_TRP_LEU = (
            (PHE_or_TRP_x - LEU_x) ** 2 + (PHE_or_TRP_y - LEU_y) ** 2 + (PHE_or_TRP_z - LEU_z) ** 2) ** 0.5
        # 判断条件
        if distance_PHE_or_TRP_LEU < distance_cutoff:
            # 若满足条件打印
            print(i, j)
            # 将满足条件的PHE/TRP的信息写入ok_PHE_or_TRP_residue
            ok_PHE_or_TRP_residue[i] = PHE_or_TRP_residue[i]
            # 将满足条件的LEU的信息写入LEU_residue
            ok_LEU_residue[j] = LEU_residue[j]
            # 满足条件的PHE/TRP以及LEU信息组成字典
            # 此处有重大bug，已改正
            # 原方法为
            # ok_dict[i]=j
            # 可能引发的问题，单个key可能存在多个满足条件的value（即满足与TRP_305距离小于7的leu存在多个），若执行ok_dict[i]=j方法只会保留最后一个value，显然是不符合预期的，我们需要都包含在内
            # 修改之后先查找字典中有没有key，没有的话写入list（里面包含LEU数据），存在的话将数据写入value列表后，再组成字典

            # 如果ok_dict中key已经存在名为i的key，追加value  本身value的数据类型为list，使用append方法添加数据
            if ok_dict.get(i):
                ok_dict[i].append(j)
            # ok_dict中key不存在名为i的key，直接写入字典，i为key ，j为value，这里[j]目的是将value的类型设置为list
            else:
                ok_dict[i] = [j]

print('/*******new_dict*******/')
# print(ok_LEU_residue)
# print(len(ok_LEU_residue))
print(ok_dict)


# # print(LEU_residue)
# aaa= 'HETATM 5223  C           1     -44.375  11.383  -5.715  0.00  0.00           C  '
# aa = aaa.split()
# print(aa)
# print(len(aa))
# for i in aa:
#     print(aa.index(i),i)


distance_cutoff_GFPP = 7
# print(distance_cutoff_GFPP)
print('PHE/TRP的CG距离GFPP小于{distance_cutoff_GFPP}A,并且PHE/TRP的CG距离LEU的CG小于{distance_cutoff}A'.format(
    distance_cutoff_GFPP=distance_cutoff_GFPP, distance_cutoff=distance_cutoff))
print('/***********GFPP***********/')
for i in GFPP_residue:
    GFPP_x = GFPP_residue[i][0]
    GFPP_y = GFPP_residue[i][1]
    GFPP_z = GFPP_residue[i][2]
    for j in ok_PHE_or_TRP_residue:
        ok_PHE_or_TRP_residue_x = ok_PHE_or_TRP_residue[j][0]
        ok_PHE_or_TRP_residue_y = ok_PHE_or_TRP_residue[j][1]
        ok_PHE_or_TRP_residue_z = ok_PHE_or_TRP_residue[j][2]
        # 比较PHE与LEU的距离是否满足条件
        distance_PHE_GFPP = (
            (GFPP_x - ok_PHE_or_TRP_residue_x) ** 2 + (GFPP_y - ok_PHE_or_TRP_residue_y) ** 2 + (GFPP_z - ok_PHE_or_TRP_residue_z) ** 2) ** 0.5
        if distance_PHE_GFPP < distance_cutoff_GFPP:
            # 若满足条件打印
            # 这边找出ok_dict中key为PHE/TRP的leu列表
            LEU_ret = ok_dict[j]
            for k in LEU_ret:
                print('{i},{j},{k}'.format(i=i, j=j, k=k))
            # for k in ok_LEU_residue:
                # print('{i},{j},{k}'.format(i=i,j=j,k=k))

# 其中我的一些逻辑可能晦涩难懂，我脑回路的问题，可能有更简单的方法处理，欢迎指正
# 非常感谢测试人员的支持与理解
